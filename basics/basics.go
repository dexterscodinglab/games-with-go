package main

import (
	"fmt"
)

func main() {
	/*
		var a int8 		//  -127 - 127
		var b uint8    //  0 - 255
	*/

	/*

	//  Creates variables
	var a uint8 = 250
	var b uint8 = 10
	var result uint8

	result = a + b

	//  4 because it wraps around (because the result is above 255.)
	fmt.Println(result)  

	*/

	/* Math is not exact in go (the floating point is not 100% accurate.)
	
	a := 2.0
	b := 3.0
	result := a / b

	fmt.Printf("%.20f", result)

	*/
}
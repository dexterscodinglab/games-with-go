package main

import (
	"fmt"
)

type position struct {
	x 	int
	y 	int	
}

type badGuy struct {
	name 	string
	health	int
	pos		position
}

func addOne(x *int) {
	//  The value of x is x + 1
	*x = *x + 1
}

func whereIsBadGuy(b *badGuy) {
	x := b.pos.x
	y := b.pos.y
	fmt.Println("(",x, ",", y, ")")
}

func main() {
	x := 5
	fmt.Println(x)

	//  Pointer to an int
	var xPtr *int = &x
	fmt.Println(xPtr)

	//  This sends a copy of x 
	//  to addOne (so x is still 5)
	addOne(&x)
	fmt.Println(x)

	p := position{4,2}

	badguy := badGuy{"Jabba", 100, p}
	whereIsBadGuy(&badguy)
}
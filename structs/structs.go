package main

import (
	"fmt"
)

type position struct {
	x 	int
	y 	int	
}

type badGuy struct {
	name 	string
	health	int
	pos		position
}

func whereIsBadGuy(b badGuy) {
	x := b.pos.x
	y := b.pos.y
	fmt.Println("(",x, ",", y, ")")
}

func main() {
	p := position{4, 2}

	b := badGuy{"Jabba", 100, p}
	fmt.Println(b)
	whereIsBadGuy(b)
}

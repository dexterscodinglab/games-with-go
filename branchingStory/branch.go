package main

import (
	"bufio"
	"fmt"
	"os"
)

//  Node structure
type storyNode struct {
	text      string
	leftPath  *storyNode
	rightPath *storyNode
}

//  Function to play the story
func (node *storyNode) play() {
	fmt.Println(node.text)

	//  Keep repeating until player wins or loses
	if node.leftPath != nil && node.rightPath != nil {
		//  Create new scanner object from stdin
		scanner := bufio.NewScanner(os.Stdin)

		for {
			//  Scan the input
			scanner.Scan()
			//  Set the answer to the input
			answer := scanner.Text()

			if answer == "left" {
				//  If the input is yes, play the leftPath
				node.leftPath.play()
				//  Then break out of the loop
				break
			} else if answer == "right" {
				//  If the answer is no, play rightPath
				node.rightPath.play()
				break
			} else {
				//  Otherwise the answer is invalid
				fmt.Println("Invalid answer, valid answers: \"left\", \"right\"")
			}
		}

	}

}

func main() {
	//  Set the root of the tree
	root := storyNode{"Do you walk left or right?", nil, nil}
	//  Below the root is the winning leaf
	winning := storyNode{"Winning leaf", nil, nil}
	//  Another leaf is the losing leaf
	losing := storyNode{"The goblin catches you! You lose!", nil, nil}
	node2 := storyNode{"A goblin appears! Do you run left or right?", nil, nil}
	node3 := storyNode{"The goblin keeps chasing you! Left or right?", nil, nil}
	root.leftPath = &node2
	root.rightPath = &losing
	node2.leftPath = &losing
	node2.rightPath = &node3
	node3.leftPath = &winning
	node3.rightPath = &losing
	root.play()
	//testing.play()

}

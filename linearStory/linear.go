package main

// TODO add function to insert new page after given page
// TODO add function to delete a page

import (
	"fmt"
)

type storyPage struct {
	text     string
	nextPage *storyPage
}

func (page *storyPage) playStory() {
	//  As long as page isn't a nil pointer...
	for page != nil {
		fmt.Println(page.text)
		page = page.nextPage
	}

}

func (page *storyPage) addToEnd(text string) {
	for page.nextPage != nil {
		page = page.nextPage
	}
	page.nextPage = &storyPage{text, nil}
}

func (page *storyPage) addAfter(text string) {
	newPage := &storyPage{text, page.nextPage}
	page.nextPage = newPage
}

func main() {
	page := storyPage{"Once upon a time...", nil}
	page.addToEnd("There was a goblin.")
	page.addToEnd("Lonely he sat on a rock...")
	page.addAfter("Testing addafter")
	//  next page of page 1 is the address of page 2.
	page.playStory()

}
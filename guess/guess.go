package main

import (
	"fmt"
	"bufio"
	"os"
)

func main() {

	//  Create a new scanner object
	scanner := bufio.NewScanner(os.Stdin) //  Bufio reads from input sources

	low := 1
	high := 100
	tries := 0

	fmt.Println("Welcome to this guessing game! Please choose a number between", low, "and", high)
	fmt.Println("Press ENTER when ready!")

	//  Read input...
	scanner.Scan()

	//  Infinite loop
	for {
		//  Guess is the middle of low and high (binary search strategy).
		guess := (low + high) / 2
		
		fmt.Println("I guess the number is: ", guess)
		fmt.Println("Is that: ")
		fmt.Println("A. Too high")
		fmt.Println("B. Too low")
		fmt.Println("C. Correct")

		//  Read user input
		scanner.Scan()

		//  The response is the user input
		response := scanner.Text()
	
		//  If the guess is too high...
		if response == "A" || response == "a" {
			//  Sets the new high to -1 than the previous middle:
			//
			//  low-----------x-----------high
			//  			to
			//  low----x----high..............
			//
			if guess > low {
				tries++
				high = guess - 1	
			} else {
				fmt.Printf("You're cheating! Your number is below %v!\n", low)
			}
			

		//  If the guess is too low...
		} else if response == "B" || response == "b" {
			//  Same as above but reverse:
			//
			//  low-----------x-----------high
			//  			to
			//  ..............low----x-----high
			//
			if guess < high {
				tries++
				low = guess + 1	
			} else {
				fmt.Printf("You're cheating! Your number is above %v!\n", high)
			}
			

		//  If the guess is right...
		} else if response == "C" || response == "c" {
			if tries == 1 {
				fmt.Println("I won! It took me", tries, "try to win!")	
			} else {
				fmt.Println("I won! It took me", tries, "tries to win!")	
			}
			//  Exit the loop
			break

		//  If the guess is invalid...
		} else {
			fmt.Println("That's not a valid response...")
		}
	}
	
}
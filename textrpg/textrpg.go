package main

//  TODO add NPCs
//  TODO make NPC move around graph
//  TODO add items to dungeon
//  TODO accept natural language as input

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type NPC struct {
	desc   string
	health int
}

type choices struct {
	cmd         string     //  The command itself
	description string     //   What are the choices?
	nextNode    *storyNode //    Linked list to next node
	nextChoice  *choices   //     Linked list to next choice
}

type storyNode struct {
	text    string   //  Each node has text
	choices *choices //   Each node have choices

}

//  Function to add choices
func (node *storyNode) addChoice(cmd string, description string, nextNode *storyNode) {
	choice := &choices{cmd, description, nextNode, nil}

	//  If the current nodes choices is empty
	if node.choices == nil {
		//  Add the choice we specified
		node.choices = choice
		//  Otherwise
	} else {
		currentChoice := node.choices
		//  While the current choice is empty
		for currentChoice.nextChoice != nil {
			//  Iterate through the choices
			currentChoice = currentChoice.nextChoice
		}
		//  When we're at the last item of the list...
		//  Set the next choice to the choice specified
		currentChoice.nextChoice = choice
	}
}

//  Function to describe a room
func (node *storyNode) render() {
	//  Print the node text
	fmt.Println(node.text)
	//  Set the current choice to the node choices
	currentChoice := node.choices
	//  While the choice isn't empty
	for currentChoice != nil {
		//  Show each command and it's description
		fmt.Println(currentChoice.cmd, ":", currentChoice.description)
		//  Then set the current choice to the next choice
		currentChoice = currentChoice.nextChoice
	}
}

//  Function to check if command is valid etc., returns next story node
func (node *storyNode) executeCmd(cmd string) *storyNode {
	currentChoice := node.choices
	for currentChoice != nil {
		//  If the command already exists (that is, is valid)
		if strings.ToLower(currentChoice.cmd) == strings.ToLower(cmd) {
			//  Return the player to the next node
			return currentChoice.nextNode
		}
		currentChoice = currentChoice.nextChoice
	}
	//  If the choice isn't valid
	fmt.Println("Sorry, I didn't understand that.")
	//  Return the player to the same (current) node
	return node
}

//  Create permanent scanner
var scanner *bufio.Scanner

//  Function to play the story
func (node *storyNode) play() {
	//  Render the current node
	node.render()
	//  If there are any options
	if node.choices != nil {
		//  Take user input
		scanner.Scan()
		//  Execute the user command (recursively play
		//  the next node since it returns one)
		node.executeCmd(scanner.Text()).play()
	}
}

func main() {
	scanner = bufio.NewScanner(os.Stdin)

	start := storyNode{text: `
	"You wake up in a dark room. 
	You hear running water nearby.
	What do you do?
	`}

	darkRoom := storyNode{text: "The room is pitch black, you can't see anything"}
	darkRoomLit := storyNode{text: "The lantern fills the room with light, illuminating it fully."}

	goblin := storyNode{text: "While looking around, a goblin appeared!"}
	trap := storyNode{text: "Suddenly a trapdoor opens below you, and you start falling!"}

	treasure := storyNode{text: "A vivid and colorful chest stands in front of you. You open it, and it's full of gold!"}

	start.addChoice("N", "Go North", &darkRoom)
	start.addChoice("S", "Go South", &darkRoom)
	start.addChoice("E", "Go East", &trap)

	darkRoom.addChoice("S", "Try to go back south", &goblin)
	darkRoom.addChoice("O", "Turn on lantern", &darkRoomLit)
	darkRoomLit.addChoice("N", "Go North", &treasure)
	darkRoomLit.addChoice("S", "Go South", &start)
	start.play()

	fmt.Println()
	fmt.Println("The end.")
}

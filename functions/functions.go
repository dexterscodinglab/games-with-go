package main

import (
	"fmt"
)

//  Function to print hello to name
func sayHello(name string) {
	fmt.Println("Hello",name)
}

//  Function to say goodbye to name
func sayGoodbye(name string) {
	fmt.Println("Goodbye",name)
}

//  Automatically greets and farwells name
func beSocial(name string) {
	sayHello(name)
	sayGoodbye(name)
}

func addOne(x int) int {
	return x+1
}

func sayHelloABunch() {
	fmt.Println("Hello")
	sayHelloABunch()
}


//  Main entrypoint
func main() {

	beSocial("bob")

	x := 5
	x = addOne(x)
	fmt.Println(x) //  6

	x = addOne(addOne(x))	
	fmt.Println(x) //  8

	sayHelloABunch()

}